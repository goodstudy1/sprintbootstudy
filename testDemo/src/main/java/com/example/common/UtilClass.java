package com.example.common;
import java.util.ArrayList;
import java.util.List;

public final class UtilClass {

	public static List<String> getProvince(){
		List<String> provinceList = new ArrayList<String>();
		provinceList.add("千葉県");
		provinceList.add("東京");
		provinceList.add("神奈川県");
		
		return provinceList;
	}
	
	public static List<String> getTableHeader(){
		List<String> tableHeader = new ArrayList<String>();
		tableHeader.add("id");
		tableHeader.add("name");
		tableHeader.add("sex");
		tableHeader.add("age");
		tableHeader.add("area");
		tableHeader.add("type");
		tableHeader.add("money");
		
		return tableHeader;
	}
}
