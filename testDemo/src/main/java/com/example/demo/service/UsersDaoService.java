package com.example.demo.service;

import java.util.List;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.example.demo.entity.Users;
import com.example.demo.mapper.UsersMapper;

@Repository
public class UsersDaoService {
	
	@Autowired
	private UsersMapper usersMapper;
	
	public List<Users> getUsersByArea(String area){
		List<Users> users = usersMapper.findByState(area);
		return users;
	}
}
