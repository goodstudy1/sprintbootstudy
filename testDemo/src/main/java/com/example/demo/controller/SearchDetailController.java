package com.example.demo.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

import com.example.form.SearchScreenForm;



@Controller
public class SearchDetailController {

	@Autowired
	private HttpSession httpSession;

	@RequestMapping("/detail")
	public ModelAndView init(String id) {
		ModelAndView modelAndView = new ModelAndView("detail");
//		SearchScreenForm searchScreenForm = new SearchScreenForm();
		SearchScreenForm searchScreenForm  = (SearchScreenForm) httpSession.getAttribute("SearchScreenForm");

		return modelAndView;
	}
}
